https://itsfoss.com/things-to-do-after-installing-ubuntu-18-04/

# Disable Switchable Graphics in Dell BIOS
* change driver to NVidia
* set repeat keys in Settings > Universal Access > Typing

# Install
* git
* curl, wget, ssh
* vim, gvim
* zsh
    - zsh-autosuggestions
    - zsh-syntax-highlighting
    - zsh-completions
    - POWERLEVEL9K
        -NerdFonts
* guake?
* cmake
* git and .gitconfig
* diff-so-fancy
* bat
* ripgrep, fd
* lsd, exa
* vscode
* firefox + treestyle tabs + hide tabs
* pip3


```
sudo apt install flatpak
```

# allow Canonical Partners Other Software in Software and Updates

# Security WiFi
- Security: WPA & WPA2 Enterprise
- Authentication: Protected EAP (PEAP)
- CA certificate: No CA certificate is required
- PEAP version: Automatic
- Inner authentication: MSCHAPv2
