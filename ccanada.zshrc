# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# # confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

# zsh-completions to completions path
fpath=(~/zsh/zsh-completions/src/ $fpath)
fpath=(~/zsh/zsh-completions $fpath)
fpath=(~/.zfunc $fpath)

# Ensure completions are available
autoload -U compinit && compinit
zmodload -i zsh/complist


# Various completion optimizations from https://github.com/robbyrussell/oh-my-zsh/blob/master/lib/completion.zsh
# Ensure highlighting when tabbing through completions
# https://stackoverflow.com/questions/46939906/zsh-tab-completion-not-working
unsetopt menu_complete   # do not autoselect the first completion entry
unsetopt flowcontrol
setopt auto_menu         # show completion menu on successive tab press
setopt complete_in_word
setopt always_to_end

zstyle ':completion:*:*:*:*:*' menu select
zstyle ':completion:*' matcher-list 'm:{a-zA-Z-_}={A-Za-z_-}' 'r:|=*' 'l:|=* r:|=*'



# ========================================================================== #
#                               Zsh Options                                  #
# ========================================================================== #
HISTFILE=~/.zsh_history
HISTSIZE=10000
SAVEHIST=5000
setopt APPEND_HISTORY
setopt INC_APPEND_HISTORY
setopt HIST_IGNORE_DUPS
setopt HIST_IGNORE_SPACE
setopt SHARE_HISTORY

# CASE_SENSITIVE="true" # case-sensitive auto-completion
# HYPHEN_INSENSITIVE="true" # make _ and - interchangeable in completions (requires case-sensitive completion off)
# DISABLE_AUTO_UPDATE="true"
# export UPDATE_ZSH_DAYS=13
# ENABLE_CORRECTION="true" # enable auto-correction of commands
# COMPLETION_WAITING_DOTS="true" # display red dots while waiting for completion.
HIST_STAMPS="yyyy-mm-dd"

# This makes repository status check for large repositories much, much faster.
DISABLE_UNTRACKED_FILES_DIRTY="true"



# ========================================================================== #
#                   Prompt customizations via POWERLEVEL9K                   #
# ========================================================================== #

# Line below must go **BEFORE** sourcing the theme
POWERLEVEL9K_MODE='nerdfont-complete'
source ~/zsh/powerlevel10k/powerlevel10k.zsh-theme
POWERLEVEL9K_PROMPT_ON_NEWLINE=true
POWERLEVEL9K_PROMPT_ADD_NEWLINE=true
POWERLEVEL9K_RPROMPT_ON_NEWLINE=false
POWERLEVEL9K_MULTILINE_FIRST_PROMPT_PREFIX=""
POWERLEVEL9K_MULTILINE_LAST_PROMPT_PREFIX="%K{238}%F{253} $ %f%k%F{238}%f "
POWERLEVEL9K_SHORTEN_STRATEGY='truncate_from_right'
POWERLEVEL9K_SHORTEN_DELIMITER=".."
POWERLEVEL9K_SHORTEN_DIR_LENGTH=4
POWERLEVEL9K_LEFT_PROMPT_ELEMENTS=(context dir dir_writable pyenv virtualenv vcs)
# POWERLEVEL9K_RIGHT_PROMPT_ELEMENTS=(command_execution_time status root_indicator background_jobs newline)
POWERLEVEL9K_RIGHT_PROMPT_ELEMENTS=(status root_indicator background_jobs newline)
POWERLEVEL9K_COMMAND_EXECUTION_TIME_BACKGROUND='black'
POWERLEVEL9K_COMMAND_EXECUTION_TIME_FOREGROUND='yellow'
POWERLEVEL9K_COMMAND_EXECUTION_TIME_PRECISION=0
POWERLEVEL9K_STATUS_ERROR_BACKGROUND='black'
POWERLEVEL9K_STATUS_ERROR_FOREGROUND='red'
POWERLEVEL9K_CARRIAGE_RETURN_ICON=''

# https://github.com/bhilburn/powerlevel9k#vcs
# POWERLEVEL9K_SHOW_CHANGESET=true

ZSH_THEME="powerlevel9k/powerlevel9k"
DEFAULT_USER=`whoami`


# ========================================================================== #
#                      oh-my-zsh plugin replacements                         #
# ========================================================================== #
# colored-man-pages (copy-paste of function in plugin)
function man() {
    env \
        LESS_TERMCAP_mb=$(printf "\e[1;31m") \
        LESS_TERMCAP_md=$(printf "\e[33m") \
        LESS_TERMCAP_me=$(printf "\e[0m") \
        LESS_TERMCAP_se=$(printf "\e[0m") \
        LESS_TERMCAP_so=$(printf "\e[1;41m") \
        LESS_TERMCAP_ue=$(printf "\e[0m") \
        LESS_TERMCAP_us=$(printf "\e[1;32m") \
        PAGER="${commands[less]:-$PAGER}" \
        _NROFF_U=1 \
        PATH="$HOME/bin:$PATH" \
            man "$@"
}


# ========================================================================== #
#                                Keybinds                                    #
# ========================================================================== #
bindkey '^ ' autosuggest-accept


# ========================================================================== #
#                                 Aliases                                    #
# ========================================================================== #
ripgrep_short() { command rg "$@" --context 2 -M 120 }
findfiles() { find . -iname "*$1*" }
dirsizes() { if [[ -z "$1" ]]; then du -h -d 1; else du -sh "$1"; fi }

# ============================ Applications ================================ #
alias top='htop'


# ============================  Navigation  ================================ #
# alias ls='exa --grid --group-directories-first --sort=name'
# alias la='exa -alhg --tree --group-directories-first --sort=Extension -L 1'
# lsd
alias ls='lsd --group-dirs first --color never -F'
alias l='lsd --group-dirs first --color never -aF'
alias la='lsd --group-dirs first --color never -laF'
alias lla='lsd --group-dirs first -laF'
alias lt='lsd --tree --depth 2 --group-dirs first --color never -aF'

alias ..='cd ..'
alias ...='cd ../..'
alias ....='cd ../../..'
alias .....='cd ../../../..'

# ============================    Utils     ================================ #
alias zshconfig="vim ~/.zshrc"
alias reload="source ~/.zshrc"
alias dirsize='dirsizes'
alias checkports='lsof -P -i -n'
alias bat='bat --theme "Agila Oceanic Next"'
alias fd='fd --hidden --no-ignore'
alias md='mkdir -p'

# ============================     git      ================================ #
commit_file() {
    git commit -m "$1"
}

# make it so if lots of files diffed, n/N go between files
diff_by_file() {
    git --no-pager diff --color $@ | diff-so-fancy | less --tabs=4 --pattern '^(Date|added|deleted|modified): '
}

alias g='git'
alias gs='git status'
alias gst='git status'

# handy faster adding tricks
alias gsu="git status --short | grep '??' | cut -c 4-"  # parseable list of unstaged files for piping
alias gsm='git status --short | grep ^.M | cut -c 4-' # parseable list of modified files for piping
alias gsd='git status --short | grep ^.D | cut -c 4-' # parseable list of deleted files for piping
alias ga='git add'
alias gam='git status --short | grep ^.M | cut -c 4- | xargs git add && git status'  # add all modified
alias gad='git status --short | grep ^.D | cut -c 4- | xargs git add && git status'  # add all deleted

alias gd='git diff'
alias gdc='git diff --cached'
alias gds='git diff --staged'
alias gdw='git diff --color-words'
alias gdf=diff_by_file

alias gcm='commit_file'
alias gcam='git commit -a -m'
alias gp='git push'
alias gb='git branch'

alias gg='git graph'
alias gl='git lg'
alias glg='git lgraph'
alias ggl='git log --graph --oneline --decorate --all'

export LESS="-RFX"




# ========================================================================== #
#                              PATH exports                                  #
# ========================================================================== #
# If you come from bash you might have to change your $PATH.
export PATH=$HOME/bin:/usr/local/bin:$PATH
export PATH=$PATH:~/.cargo/bin
export PATH=$PATH:~/cmdline_binaries
export PATH=$PATH:~/bin
export PATH=$PATH:/usr/local/mysql/bin
export EDITOR=/usr/local/bin/vim

#/ .bashrc stuff

# ========================================================================== #
#                              Compute Canada                                #
# ========================================================================== #
# Compute Canada
source $LMOD_PKG/init/zsh
fpath=(/cvmfs/soft.computecanada.ca/nix/store/2zq3rwywhi6cf3gwicf2y3kj2h8ps3v8-zsh-5.2/lib/zsh/5.2/zsh $fpath)
# https://github.com/ohmyzsh/ohmyzsh/issues/4607 for Compute Canada solution below
export FPATH="/cvmfs/soft.computecanada.ca/nix/var/nix/profiles/16.09/share/zsh/version/functions:$FPATH"
export FPATH="/cvmfs/soft.computecanada.ca/nix/var/nix/profiles/16.09/share/zsh/5.2/functions:$FPATH"
export FPATH="/cvmfs/soft.computecanada.ca/nix/var/nix/profiles/16.09/share/zsh/:$FPATH"
alias interactive=salloc --time=0:45:0 --ntasks=1 --account=def-jlevman --mem=16G
alias watch_job=srun --jobid $1 --pty tmux new-session -d "htop -u $USER" \; split-window -h 'watch nvidia-smi' \; attach
# set up ssh
alias start_agent=eval `ssh-agent -s`

# ========================================================================== #
#                          Final zsh Setup Stuff                             #
# ========================================================================== #

source ~/zsh/zsh-autosuggestions/zsh-autosuggestions.zsh
export ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE='fg=8'
export ZHS_AUTOSUGGEST_STRATEGY='history'

# configure syntax-highlighting - This line MUST be last in .zshrc
source ~/zsh/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh
