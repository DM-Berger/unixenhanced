#!/bin/sh

cat ./.zshrc >> ~/.zshrc
cp ./.p10k.zsh ~/.p10k.zsh
sudo apt-get update
sudo apt-get install font-manager git curl wget vim gvim zsh cmake

# install NerdFonts
mkdir Fonts
mkdir -p ~/.fonts  # make .fonts directory if doesn't exist
unzip NerdFonts/CodeFonts.zip -d Fonts
cp Fonts/CodeFonts/* ~/.fonts
fc-cache -f -v  # rebuild font cache

# Install rust for cargo, CLI tools
curl https://sh.rustup.rs -sSf | sh
cargo install ripgrep fd-find bat lsd exa

# setup zsh
cd ~
mkdir zsh
cd zsh
git clone https://github.com/zsh-users/zsh-completions.git
git clone https://github.com/zsh-users/zsh-autosuggestions
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git
git clone https://github.com/romkatv/powerlevel10k.git

# set the default shell to zsh
chsh -s $(which zsh)
