# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

# ========================================================================== #
#                               Zsh Options                                  #
# ========================================================================== #

# add zsh-completions to completions path
fpath=(~/zsh/zsh-completions/src/ $fpath)
fpath=(~/zsh/zsh-completions $fpath)
fpath=(~/.zfunc $fpath)
# fpath=( ~/zsh_scripts/completion $fpath)

# Ensure completions are available
autoload -U compinit && compinit
zmodload -i zsh/complist


# Various completion optimizations from https://github.com/robbyrussell/oh-my-zsh/blob/master/lib/completion.zsh
# Ensure highlighting when tabbing through completions
# https://stackoverflow.com/questions/46939906/zsh-tab-completion-not-working
unsetopt menu_complete   # do not autoselect the first completion entry
unsetopt flowcontrol
setopt auto_menu         # show completion menu on successive tab press
setopt complete_in_word
setopt always_to_end

zstyle ':completion:*:*:*:*:*' menu select
zstyle ':completion:*' matcher-list 'm:{a-zA-Z-_}={A-Za-z_-}' 'r:|=*' 'l:|=* r:|=*'
# zstyle ':completion:*' menu select matcher-list 'm:{a-zA-Z}={A-Za-z}' 'r:|=*' 'l:|=* r:|=*' menu select

HISTFILE=~/.zsh_history
HISTSIZE=10000
SAVEHIST=5000
setopt APPEND_HISTORY
setopt INC_APPEND_HISTORY
setopt HIST_IGNORE_DUPS
setopt HIST_IGNORE_SPACE
setopt SHARE_HISTORY

# CASE_SENSITIVE="true" # case-sensitive auto-completion
# HYPHEN_INSENSITIVE="true" # make _ and - interchangeable in completions (requires case-sensitive completion off)
# DISABLE_AUTO_UPDATE="true"
# export UPDATE_ZSH_DAYS=13
# ENABLE_CORRECTION="true" # enable auto-correction of commands
# COMPLETION_WAITING_DOTS="true" # display red dots while waiting for completion.
HIST_STAMPS="yyyy-mm-dd"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
DISABLE_UNTRACKED_FILES_DIRTY="true"


# ========================================================================== #
#                   Prompt customizations via POWERLEVEL9K                   #
# ========================================================================== #

# Line below must go **BEFORE** sourcing the theme
POWERLEVEL9K_MODE='nerdfont-complete'
source /home/derek/zsh/powerlevel10k/powerlevel10k.zsh-theme
POWERLEVEL9K_PROMPT_ON_NEWLINE=true
POWERLEVEL9K_PROMPT_ADD_NEWLINE=true
POWERLEVEL9K_RPROMPT_ON_NEWLINE=false
POWERLEVEL9K_MULTILINE_FIRST_PROMPT_PREFIX=""
POWERLEVEL9K_MULTILINE_LAST_PROMPT_PREFIX="%K{238}%F{253} $ %f%k%F{238}\uE0B0%f "
POWERLEVEL9K_SHORTEN_STRATEGY='truncate_from_right'
POWERLEVEL9K_SHORTEN_DELIMITER=".."
POWERLEVEL9K_SHORTEN_DIR_LENGTH=4
# POWERLEVEL9K_LEFT_PROMPT_ELEMENTS=(context dir dir_writable vcs)
VIRTUAL_ENV_DISABLE_PROMPT=1  # ensures virtualenv prompt doesn't interfere with below
POWERLEVEL9K_LEFT_PROMPT_ELEMENTS=(context dir dir_writable pyenv virtualenv vcs)
# POWERLEVEL9K_RIGHT_PROMPT_ELEMENTS=(command_execution_time status root_indicator background_jobs newline)
POWERLEVEL9K_RIGHT_PROMPT_ELEMENTS=(status root_indicator background_jobs newline)
POWERLEVEL9K_COMMAND_EXECUTION_TIME_BACKGROUND='black'
POWERLEVEL9K_COMMAND_EXECUTION_TIME_FOREGROUND='yellow'
POWERLEVEL9K_COMMAND_EXECUTION_TIME_PRECISION=0
POWERLEVEL9K_STATUS_ERROR_BACKGROUND='black'
POWERLEVEL9K_STATUS_ERROR_FOREGROUND='red'
POWERLEVEL9K_CARRIAGE_RETURN_ICON="\uF443"

# https://github.com/bhilburn/powerlevel9k#vcs
# POWERLEVEL9K_SHOW_CHANGESET=true

ZSH_THEME="powerlevel9k/powerlevel9k"
DEFAULT_USER=`whoami`


# ========================================================================== #
#                      oh-my-zsh plugin replacements                         #
# ========================================================================== #
# colored-man-pages (copy-paste of function in plugin)
function man() {
    env \
        LESS_TERMCAP_mb=$(printf "\e[1;31m") \
        LESS_TERMCAP_md=$(printf "\e[33m") \
        LESS_TERMCAP_me=$(printf "\e[0m") \
        LESS_TERMCAP_se=$(printf "\e[0m") \
        LESS_TERMCAP_so=$(printf "\e[1;41m") \
        LESS_TERMCAP_ue=$(printf "\e[0m") \
        LESS_TERMCAP_us=$(printf "\e[1;32m") \
        PAGER="${commands[less]:-$PAGER}" \
        _NROFF_U=1 \
        PATH="$HOME/bin:$PATH" \
            man "$@"
}

export WARP_CONFIG_FILE='/home/derek/zsh/warp/.warp.config'
wd() {
    local arg1=$1
    # just passed in a flag like --list, --check, --clean, --help
    if [[ $# == 1 && ${arg1:0:2} == "--" ]];
    then
        /home/derek/zsh/warp/warp $@
        # cargo run -- $@
        # command wd $@
        return
    fi;

    # either wd --add name, wd --remove name, wd -a name, wd -r name
    if [[ $# == 2 ]];
    then
        /home/derek/zsh/warp/warp $@
        # cargo run -- $@
        # command wd $@
        return
    fi;

    if [[ $# == 1 ]];
    then
        local dir=$(/home/derek/zsh/warp/warp $@)
        if [[ ! -z $dir ]];
        then
            cd "$dir" # double quotes necessay, path should have unescaped spaces
            return
        fi;
    fi;
    # /Users/derek/zsh_scripts/warp/warp $@
}


# ========================================================================== #
#                            zsh fuzzy finder                                #
# ========================================================================== #
# source /Users/derek/zsh-plugins/zsh-fzy/zsh-fzy.plugin.zsh
# 
# zstyle :fzy:file    prompt       ' >> '
# zstyle :fzy:file    lines        '10'
# zstyle :fzy:file command rg --files


# ========================================================================== #
#                                Keybinds                                    #
# ========================================================================== #
bindkey '^ ' autosuggest-accept
# bindkey '^F'  fzy-cd-widget
# bindkey '^F'  fzy-file-widget


# ========================================================================== #
#                                 Aliases                                    #
# ========================================================================== #

# ======================= Alias Helper Functions =========================== #
#
ripgrep_short() { command rg "$@" --context 2 -M 120 }
findfiles() { find . -iname "*$1*" }
dirsizes() { if [[ -z "$1" ]]; then du -h -d 1; else du -sh "$1"; fi }

summarize_nii() {
    python3 -W ignore::DeprecationWarning ~/summarize_nii.py $1;
}

matlab_run() {
    matlab -nodisplay -nosplash -nodesktop -r "run(\"$1\");exit;"
}

mrun() {
    matlab -nodisplay -nosplash -nodesktop -r "try;run(\"$1\");catch e;warning(e.message);disp(e.stack(1));disp(e.stack(2));end;exit;" | tail -n +11
}

cp_configs() {
    cp -r ~/Documents/CSCI444/configs/. $1
}

# ============================ Applications ================================ #
alias vim='gvim'
alias top='htop'
alias python3.8.5='~/.pyenv/versions/3.8.5/bin/python3.8'
alias python3.8='~/.pyenv/versions/3.8.5/bin/python3.8'

# ============================  Locations   ================================ #
alias desk="cd ~/Desktop"
alias dev="cd ~/dev/"

# ============================  Navigation  ================================ #
# exa (mostly defunct)
# alias ls='exa --grid --group-directories-first --sort=name'
alias la='exa -alhg --tree --group-directories-first --sort=Extension --git -L 1'
alias l='exa -alhg --tree --group-directories-first --sort=Extension -L 1'

# lsd
alias ls='lsd --group-dirs first --color never -F'
# alias la='lsd --group-dirs first --color never -laF'
alias lla='lsd --group-dirs first -laF'
alias lt='lsd --tree --depth 2 --group-dirs first --color never -aF'

alias ..='cd ..'
alias ...='cd ../..'
alias ....='cd ../../..'
alias .....='cd ../../../..'

# ============================    Utils     ================================ #
alias zshconfig="gvim ~/.zshrc"
alias reload="source ~/.zshrc"
alias findfile=findfiles
alias rg=ripgrep_short
alias dirsize='dirsizes'
alias checkports='lsof -P -i -n'
alias fd='fd --hidden --no-ignore'
alias md='mkdir -p'
alias zotero='~/.zotero/Zotero-5.0.66_linux-x86_64/Zotero_linux-x86_64/zotero'
alias open="xdg-open"
alias pbcopy='xsel --clipboard --input'
alias pbpaste='xsel --clipboard --output'
alias nettop=nethogs
alias globus_connect='/home/derek/globusconnectpersonal-2.3.9/globusconnectpersonal -start &'
alias globus_restart='/home/derek/globusconnectpersonal-2.3.9/globusconnectpersonal -stop && /home/derek/globusconnectpersonal-2.3.9/globusconnectpersonal -start &'

# ============================     git      ================================ #
commit_file() {
    git commit -m "$1" && git status
}

# make it so if lots of files diffed, n/N go between files
diff_by_file() {
    git --no-pager diff --color $@ | diff-so-fancy | less --tabs=4 --pattern '^(Date|added|deleted|modified): '
}

alias g='git'
alias gs='git status'
alias gst='git status'

alias g='git'
alias gs='git status'
alias gst='git status'

# handy faster adding tricks
alias gsu="git status --short | grep '??' | cut -c 4-"  # parseable list of unstaged files for piping
alias gsm='git status --short | grep ^.M | cut -c 4-' # parseable list of modified files for piping
alias gsd='git status --short | grep ^.D | cut -c 4-' # parseable list of deleted files for piping
alias ga='git add'
alias gam='git status --short | grep ^.M | cut -c 4- | xargs git add && git status'  # add all modified
alias gad='git status --short | grep ^.D | cut -c 4- | xargs git add && git status'  # add all deleted

alias gd='git diff'
alias gdc='git diff --cached'
alias gds='git diff --staged'
alias gdw='git diff --color-words'
alias gdf=diff_by_file

alias gcm='commit_file'
alias gcam='git commit -a -m'
alias gp='git push'
alias gb='git branch'

alias gg='git graph'
alias gl='git lg'
alias glg='git lgraph'
alias ggl='git log --graph --oneline --decorate --all'

export LESS="-RFX"


# ========================================================================== #
#                           Dirty alias tricks                               #
# ========================================================================== #
# re-enable below if you need ruby, this is slowing zsh startup
# source /Users/derek/.rvm/scripts/rvm
# alias loadrvm='[[ -s "$HOME/.rvm/scripts/rvm" ]] && . "$HOME/.rvm/scripts/rvm"'

# attempt to speed up slow shell creation caused by NVM
# https://github.com/creationix/nvm/issues/539#issuecomment-245791291
# NOTE: Line for bash-completion directly below seems not to be needed with zsh
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh" --no-use  # loads nvm
# [ -s "$NVM_DIR/bash_completion" ] && . "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

nvm_aliases='npm node tldr tsc web-ext'
alias node="unalias $nvm_aliases; nvm use default; node $@"
alias npm="unalias $nvm_aliases; nvm use default ; npm $@"
alias tldr="unalias $nvm_aliases; nvm use default ; tldr $@"
alias tsc="unalias $nvm_aliases; nvm use default ; tsc $@"
alias web-ext="unalias $nvm_aliases; nvm use default ; web-ext $@"
alias python4='python3'

# speed up zsh startup by hacky lazy loading br
alias br='unalias br; source /home/derek/.config/broot/launcher/bash/br; br $@'
# source /Users/derek/Library/Preferences/org.dystroy.broot/launcher/bash/br  # MacOS
# source /home/derek/.config/broot/launcher/bash/br  # Ubuntu/Linux

# ========================================================================== #
#                              PATH exports                                  #
# ========================================================================== #
# Add RVM to PATH for scripting. Make sure this is the last PATH variable change.
# export PATH="$PATH:$HOME/.rvm/bin"
export PATH="$PATH:$HOME/.local/bin"
export PATH="$PATH:/home/derek/MATLAB2018/symlinks"
export PATH="$PATH:/home/derek/.nvm/versions/node/v12.1.0/lib"
export PATH="$PATH:/home/derek/swig/bin"
export PATH=/usr/local/cuda-10.1/bin:/usr/local/cuda-10.1/NsightCompute-2019.1${PATH:+:${PATH}}
export PATH="$PATH:/home/derek/bin/ants/bin"
export PATH="$HOME/.poetry/bin:$PATH"


# ========================================================================== #
#                     Neuroscience and Deep Learning                         #
# ========================================================================== #
export ITK_GLOBAL_DEFAULT_NUMBER_OF_THREADS=4
export ANTSPATH=/home/derek/bin/ants/bin
CUDA_HOME=${CUDA_HOME}:/usr/local/cuda:/usr/local/cuda-9.0export
LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:/usr/local/cuda-10.1/lib64export
LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/cuda/extras/CUPTI/lib64
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/cuda/extras/CUPTI/lib64
export EDITOR=/usr/bin/vim


export FREESURFER_HOME=/usr/local/freesurfer

# Globus Connect
GLOBUS_CLI_INSTALL_DIR="$(python3 -c 'import site; print(site.USER_BASE)')/bin"
export PATH="$GLOBUS_CLI_INSTALL_DIR:$PATH"
export DB_EP="77ec31b8-e6ad-11e9-9bfc-0a19784404f4"

# FSL Setup
# FSLDIR=/usr/local/fsl
# PATH=${FSLDIR}/bin:${PATH}
# export FSLDIR PATH
# . ${FSLDIR}/etc/fslconf/fsl.sh
# source $FREESURFER_HOME/SetUpFreeSurfer.sh

alias fsl_setup="FSLDIR=/usr/local/fsl;PATH=${FSLDIR}/bin:${PATH};export FSLDIR PATH;. ${FSLDIR}/etc/fslconf/fsl.sh;source $FREESURFER_HOME/SetUpFreeSurfer.sh"

cc_tensorboard() {
    if [ -z "$1" ]
    then
        echo "usage: cc_tensorboard <CC hostname>\n"
        echo "Show the list of your jobs using the command sq"
        echo "Find the job, and note the value in the 'NODELIST' column (this is the hostname). "
    else
        ssh -N -f -L localhost:6006:$1:6006 dberger@graham.computecanada.ca && xdg-open http://localhost:6006
    fi
}

# The next line updates PATH for the Google Cloud SDK.
if [ -f '/home/derek/google-cloud-sdk/path.zsh.inc' ]; then . '/home/derek/google-cloud-sdk/path.zsh.inc'; fi

# The next line enables shell command completion for gcloud.
if [ -f '/home/derek/google-cloud-sdk/completion.zsh.inc' ]; then . '/home/derek/google-cloud-sdk/completion.zsh.inc'; fi


# ========================================================================== #
#                               Python                                       #
# ========================================================================== #

export PYENV_ROOT="$HOME/.pyenv"
export PATH="$PYENV_ROOT/bin:$PATH"
export PYTHONSTARTUP="$HOME/.pythonrc"
export BETTER_EXCEPTIONS=1  # Python

# this enables shims and autocompletion for pyenv
if command -v pyenv 1>/dev/null 2>&1;
then
    eval "$(pyenv init -)"
fi

# >>> conda initialize >>>
# !! Contents within this block are managed by 'conda init' !!
# __conda_setup="$('/home/derek/miniconda3/bin/conda' 'shell.zsh' 'hook' 2> /dev/null)"
# if [ $? -eq 0 ]; then
#     eval "$__conda_setup"
# else
#     if [ -f "/home/derek/miniconda3/etc/profile.d/conda.sh" ]; then
#         . "/home/derek/miniconda3/etc/profile.d/conda.sh"
#     else
#         export PATH="/home/derek/miniconda3/bin:$PATH"
#     fi
# fi
# unset __conda_setup
# <<< conda initialize <<<

# OPAM configuration
# . /Users/derek/.opam/opam-init/init.zsh > /dev/null 2> /dev/null || true
 

# ========================================================================== #
#                          Final zsh Setup Stuff                             #
# ========================================================================== #
source /home/derek/zsh/zsh-autosuggestions/zsh-autosuggestions.zsh
export ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE='fg=8'
export ZHS_AUTOSUGGEST_STRATEGY='history'

# configure syntax-hightlinging - This line MUST be last in .zshrc
source /home/derek/zsh/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

# https://github.com/zsh-users/zsh-history-substring-search
# source /usr/local/share/zsh-history-substring-search/zsh-history-substring-search.zsh
#
# bindkey '^[[1;5A' history-substring-search-up
# bindkey '^[[1;5B' history-substring-search-down


# ========================================================================== #
#                 Handy Notes to Print with e.g. `tail .zshrc`               #
# ========================================================================== #

# speed up key repeats
# gsettings set org.gnome.desktop.peripherals.keyboard repeat-interval 7
# gsettings set org.gnome.desktop.peripherals.keyboard delay 250  # default seems to be fine

# For Terraria, to play
# xset r off 
# Then xset r on to fix normal usage

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh
